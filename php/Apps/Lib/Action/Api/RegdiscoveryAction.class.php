<?php

/* 
 * 首先感谢使用微笑开发程序，程序中如有不足，请多多指教与指导
 * 使用源代码请勿删除声明文件，辛苦开源贡献代码不容易，请保留作者应该有的知晓权
 * 同时感谢本系统中使用的框架和其他开源作者辛苦劳动成果，感谢！！
 * 微笑交流QQ：512720913 邮箱地址：512720913@qq.com by.weixiao
 */

class RegdiscoveryAction extends Action{
    public function index(){
          header("Content-Type: text/html; charset=utf-8");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");

        //接收post或者get数据 默认已经格式化输出了
        $itemname =  urldecode( $this->_param('itemname'));
        $itemurl = $this->_param('itemurl');
        $itempriority= $this->_param('itempriority');
        $sign = $this->_param('sign');

        //判断接口参数为空定义
        if (empty($itemname)) {
            echo _tojson(1, "名称不能为空！", null);
            return;
        }
        if (empty($itempriority)) {
            echo _tojson(1, "显示优先级不能为空！", null);
            return;
        }
        if (empty($itemurl)) {
            echo _tojson(1, "URL不能为空！", null);
            return;
        }
        if (empty($sign)) {
            echo _tojson(1, "签名数据不能为空！", null);
            return;
        }
        // 开始判断签名 按照键名排序
        $prestr = $itemname . $itemurl . $itempriority ;
        if (md5Verify($prestr, $sign, C('sign_key'))) {
            //实例化部门数据表
            $Discovery = M('Discovery');
            //开始判断部门是否存在
            $return_depart = $Discovery->where(array("itemname" => $itemname))->find();
      
            if (empty($return_depart)) {

                $arr = array(
                    "itemname" => $itemname,
                    "itemurl" => $itemurl,
                    "itempriority" => $itempriority,
                    "status" => "1",
                    "created" => time(),
                );
                $return_dis = $Discovery->add($arr);
                  
                if ($return_dis !== FALSE) {

                    echo _tojson(0, "ok", $code);
                    return;
                } else {
                    echo _tojson(1, "写入数据库失败", $code);
                    return;
                }
            } else {
                echo _tojson(1, $itemname . "已存在", null);
                return;
            }
        } else {
            echo _tojson("1", "数据签名校验失败！", C('check_sige_err'));
        }
    } 
    
}