<?php

/* 
 * 首先感谢使用微笑开发程序，程序中如有不足，请多多指教与指导
 * 使用源代码请勿删除声明文件，辛苦开源贡献代码不容易，请保留作者应该有的知晓权
 * 同时感谢本系统中使用的框架和其他开源作者辛苦劳动成果，感谢！！
 * 微笑交流QQ：512720913 邮箱地址：512720913@qq.com by.weixiao
 */

class DiscoveryAction extends Action{
    public function index(){
        header("Content-Type: text/html; charset=utf-8");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");

        //接收post或者get数据 默认已经格式化输出了
        $sign = $this->_param('sign'); 
        $status = $this->_param('status');
        //判断接口参数为空定义
        if (empty($sign)) {
            echo _tojson(1, "签名数据不能为空！", null);
            return;
        }
        // 开始判断签名 按照键名排序
        $prestr = $status;
        if (md5Verify($prestr, $sign, C('sign_key'))) {
            //实例化发现数据表
            $Discovery = M('Discovery');
            $return_depart = $Discovery->field("itemName","itemUrl","itemPriority","status")->select();
          //  var_dump($return_depart);
            echo _tojson(0, $return_depart, $code);
            die;
            echo _tojson_arr(
                    array(
                    "itemName" => $return_depart['itemName'],
                    "itemUrl" =>$return_depart['itemUrl'],
                    "itemPriority" =>$return_depart['itemPriority'],
                    "status" =>$return_depart['status'],
                    ));
            
        }else {
            echo _tojson("1", "数据签名校验失败！", C('check_sige_err'));
        }
    }
}