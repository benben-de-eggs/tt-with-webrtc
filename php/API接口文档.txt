teamtalk2.0 http注册接口

传输协议： post/get
全局接口地址：http://teamtalk.p52.cn/api/
返回给事：JSON格式返回
关键参数解释：sign 签名内容  ； status 状态返回值  ；info 返回内容  ；code 返回代码

签名sign：
调用API 时需要对请求参数进行签名验证，teamtalk服务器也会对该请求参数进行验证是否合法的。方法如下：

      根据参数名称（除签名和图片）将所有请求参数按照字母先后顺序排序:key + value .... key + value
      例如：将foo=1,bar=2,baz=3 排序为bar=2,baz=3,foo=1，参数名和参数值链接后，得到拼装字符串bar2baz3foo1

      系统同时支持MD5和HMAC两种加密方式:
      md5：将secret 拼接到参数字符串头、尾进行md5加密后，再转化成大写，格式是：byte2hex(md5(secretkey1value1key2value2...secret))
      hmac：采用hmac的md5方式，secret只在头部的签名后再转化成大写，格式 是：byte2hex (hmac(key1value1key2value2..., secret))

      注：hex为自定义方法，JAVA中MD5是对字节数组加密，加密结果是16字节，我们需要的是32位的大写字符串，图片参数不用加入签名中测试 工具使用的是HMAC的加密方式。

 1、输入参数为
       timestamp=2013-05-06 13:52:03
       format=xml
       app_key=test
       v=2.0
       fields=nick
       sign_method=md5
       session=test
 2、按首字母升序排列
 app_key=test
       fields=nick
       format=xml
       method=get
       session=test
       sign_method=md5
       timestamp=2013-05-06 13:52:03
       v=2.0

        3、连接字符串
        连接参数名与参数值,并在首尾加上secret，如下：
       testapp_keytestfieldsnickformatxmlmethodgetsessiontestsign_methodmd5timestamp2013-05-06 13:52:03v2.0test  
 4、生成签名 sign
        32位大写MD5值->72CB4D809B375A54502C09360D879C64
 5、拼装API请求
        将所有参数值转换为UTF-8编码，然后拼装，通过浏览器访问该地址，即成功调用一次接口，如下（http或https网关）：

--------------------
 
    'sign_key'=>md5(date("Ymd")), //   加密私钥

***********************************************************************************



1、部门注册接口
接口提交地址：http://teamtalk.p52.cn/api/Regdepart
参数：departname，priority，parentid，sign（部门名称，显示优先级，上级部门id，数据签名）

2、用户注册接口
接口提交地址：http://teamtalk.p52.cn/api/Reguser
参数：sex,name,nick,password,phone,email,avatar,departId,sign(性别，登录名，昵称，密码，手机号码，邮箱，图像地址，部门ID,签名)

3、新增发现URL接口
接口提交地址：http://teamtalk.p52.cn/api/Regdiscovery
测试：http://teamtalk.p52.cn/api/Regdiscovery?itemname=%E6%B5%8B%E8%AF%95&itemurl=http://www.baidu.com&itempriority=1&sign=5D56B281ABFCA48E1B6C1C55AD32BAE9
参数：itemname,itemurl,itempriority,sign(名称，url地址，排序，签名)


4、查询发现url接口
接口提交地址：http://teamtalk.p52.cn/api/Discovery
测试：http://teamtalk.p52.cn/api/Discovery?sign=8E2A5525D4BBBA84BFE1F1766D22F043
参数：sign（签名）