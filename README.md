#tt-with-webrtc

share_ptr修改，最需要注意的地方是需要继承enable_shared_from_this类和在传递this指针时使用shared_from_this()
以及记得删除ReleaseRef调用，否则会二次删除导致程序崩溃。

修改文件有

base {
BaseSocket.cpp
BaseSocket.h

EventDispatch.cpp
EventDispatch.h

imconn.cpp
imconn.h

ServInfo.cpp
ServInfo.h

}

msg_server{

msg_server.cpp
DBServConn.cpp
DBServConn.h

FileHandler.cpp
FileHandler.h

GroupChat.cpp
GroupChat.h

MsgConn.cpp
MsgConn.h
}

balance_server {

balance_server.cpp
balance_server.h
HttpConn.cpp
HttpConn.h
BalanceConn.cpp
BalanceConn.h

}

以上列出有改动的模块，其他暂未动，其他模块可以做个比对，

需要改动的地方其实不是特别多，但上面提到的两个注意点需要注意一下。

我用自己写的client模块测了一下注册和登陆功能，目前未发现异常。


